INTRODUCTION
------------
This simple module will provide a way to alter all file download paths slighty
to avoid some file caching systems to prevent blocking access to new file
versions. This module will add a checkbox to file field configuration page.

Use case
--------
Some aggressive file caching may cache files based on the filename, failing to
refresh the file when it is being updated in Drupal filesystem. This may happen
when users are allowed to overwrite existing files with new versions instead of
adding new files with new names (new version of file.pdf becomes file_0.pdf).

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
* Edit entity's file field configuration, for nodes Admin » Structure » Content
types » TYPE » Manage fields » FILE FIELD » Edit  
or http://example.com/admin/structure/types/manage/TYPE/fields/FILE_FIELD.
* Check "Alter file download links"
* Download links have now timestamp added to the end of file download links
(URI).

MAINTAINERS
-----------
Current maintainer:
 * Perttu Ehn (rpsu) - https://drupal.org/user/102121
